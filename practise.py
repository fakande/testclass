
class User:
    def __init__(self, name):
        self.name = name
        self.__password = ""

    @property # getter
    def password(self):
        # password must not contain numbers 0-9 ?
        if len(self.__password) == 0:
            return ""
        # res = self.__password[0] + '*' * (len(self.__password) - 2) + self.__password[-1]

        secret = self.__password[0]
        for i in range(len(self.__password) - 2):
            secret += "*"
        secret += self.__password[-1]
        return secret

    @password.setter
    def password(self, new_pass):
        if len(new_pass) < 6:
            for i in range(6 - len(new_pass)):
                new_pass += "#"
        self.__password = new_pass


    @password.deleter
    def password(self):
        del self.__password


u = User("John")
u.password = "s"
print(u.password)
u.password = "L0nger passwords ArE M0r3 Secure 0r s0 th3y s@y!"
print(u.password)
